# Example:

```bash
helm upgrade --install --atomic gitlab-runner gitlab-runner-dind/charts/gitlab-runner-0.43.1.tgz -f gitlab-runner-dind/values.yaml --set runnerRegistrationToken="<your_token>"
```